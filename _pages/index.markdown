---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
permalink: /

tagline: "un blog qualunque su una scuola qualunque"
header:
  overlay_image: /assets/images/homeHeader.png
  caption: "Photo credit: [**Unsplash**](https://unsplash.com)"

---
